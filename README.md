### Hello 👋 I am The Engineer
### Hola 👋 Yo soy El Ingeniero

<div id="badges">
  <a href="https://www.linkedin.com/in/ernest-umerov-7bb49a1b0/">
    <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
  </a>
</div>

## Technology stack

**Operation System**  

![Linux](https://img.shields.io/static/v1?style=for-the-badge&message=Linux&color=222222&logo=Linux&logoColor=FCC624&label=)
![RedHat](https://camo.githubusercontent.com/770f805629afea44247b7da039c7cca697f085f233d0fdc9bdd0ba5635c1b22f/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d5265642b48617426636f6c6f723d454530303030266c6f676f3d5265642b486174266c6f676f436f6c6f723d464646464646266c6162656c3d)
![Ubuntu](https://camo.githubusercontent.com/1814dfdb62c9a3366a9946083ac0f3ed32aad98e665b287769332252d945f2f1/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d5562756e747526636f6c6f723d453935343230266c6f676f3d5562756e7475266c6f676f436f6c6f723d464646464646266c6162656c3d)


**IaaC**

![Ansible](https://camo.githubusercontent.com/ab66bd562a57bf746d47b941f2380018843764982db6f3c51a511a13d7b1bb71/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d416e7369626c6526636f6c6f723d454530303030266c6f676f3d416e7369626c65266c6f676f436f6c6f723d464646464646266c6162656c3d)
![Terraform](https://camo.githubusercontent.com/786a09482fe7a0dacfe7bd1b6660157abaecd710b4e923f7938af1b937d143cd/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d5465727261666f726d26636f6c6f723d374234324243266c6f676f3d5465727261666f726d266c6f676f436f6c6f723d464646464646266c6162656c3d)


**Databases**

![PostgreSQL](https://img.shields.io/static/v1?style=for-the-badge&message=PostgreSQL&color=4169E1&logo=PostgreSQL&logoColor=FFFFFF&label=)
![SQLite](https://camo.githubusercontent.com/396f0a964cc4e5ad0dfc311bb2abaf8bae3acfc1458cef13f7882aa9bb11b693/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d53514c69746526636f6c6f723d303033423537266c6f676f3d53514c697465266c6f676f436f6c6f723d464646464646266c6162656c3d)
![MongoDB](https://camo.githubusercontent.com/eb3676422a9e186ce18237e6c1ffee703068f7850c2a513b9a261f33ee335ed6/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d4d6f6e676f444226636f6c6f723d343741323438266c6f676f3d4d6f6e676f4442266c6f676f436f6c6f723d464646464646266c6162656c3d)

**Containers**

![Docker](https://img.shields.io/static/v1?style=for-the-badge&message=Docker&color=2496ED&logo=Docker&logoColor=FFFFFF&label=)
![Podman](https://img.shields.io/static/v1?style=for-the-badge&message=Podman&color=892CA0&logo=Podman&logoColor=FFFFFF&label=)
![Kubernetes](https://img.shields.io/static/v1?style=for-the-badge&message=Kubernetes&color=326CE5&logo=Kubernetes&logoColor=FFFFFF&label=)


**CI\CD**

![Jenkins](https://img.shields.io/static/v1?style=for-the-badge&message=Jenkins&color=D24939&logo=Jenkins&logoColor=FFFFFF&label=)
![GitLab](https://img.shields.io/static/v1?style=for-the-badge&message=GitLab&color=FC6D26&logo=GitLab&logoColor=FFFFFF&label=)
![GitHub Actions](https://img.shields.io/static/v1?style=for-the-badge&message=GitHub+Actions&color=2088FF&logo=GitHub+Actions&logoColor=FFFFFF&label=)


**Logging**

![Elastic Stack](https://img.shields.io/static/v1?style=for-the-badge&message=Elastic+Stack&color=005571&logo=Elastic+Stack&logoColor=FFFFFF&label=)

**Web Framework**

![FastAPI](https://camo.githubusercontent.com/81b1b79330b1154fc0743b25327cbfd6282a7bf37e8d0b48278dc57528b2517c/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d4661737441504926636f6c6f723d303039363838266c6f676f3d46617374415049266c6f676f436f6c6f723d464646464646266c6162656c3d)
![Flask](https://camo.githubusercontent.com/31dfe5f167d56ccab3ca37634bf1d396e48231856b25576b5dafbc934bd327e9/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d466c61736b26636f6c6f723d303030303030266c6f676f3d466c61736b266c6f676f436f6c6f723d464646464646266c6162656c3d)

**Hardware**

![Raspberry PI](https://camo.githubusercontent.com/65a7326c61a9396cbd851c2410345175c3821061198b4f6058a9c338f0ace75c/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d5261737062657272792b506926636f6c6f723d413232383436266c6f676f3d5261737062657272792b5069266c6f676f436f6c6f723d464646464646266c6162656c3d)
![Arduino](https://camo.githubusercontent.com/f7ffc36eaeea46ca44c7a8d9ecf6c378b7ff6ff102c6ea724cf2e1ae5d04aee8/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d41726475696e6f26636f6c6f723d303039373944266c6f676f3d41726475696e6f266c6f676f436f6c6f723d464646464646266c6162656c3d)
![SEGA](https://camo.githubusercontent.com/0398dcb22a721d8849d138c01368a5f9974f4e3dbe62ce04526b437050c0743c/68747470733a2f2f696d672e736869656c64732e696f2f7374617469632f76313f7374796c653d666f722d7468652d6261646765266d6573736167653d5365676126636f6c6f723d303038394346266c6f676f3d53656761266c6f676f436f6c6f723d464646464646266c6162656c3d)


## Statistics

![CoolCoderCarl github stats](https://github-readme-stats.vercel.app/api?username=CoolCoderCarl&show_icons=true&theme=dracula&include_all_commits=true&count_private=true)

![CoolCoderCarl Languages](https://github-readme-stats.vercel.app/api/top-langs/?username=CoolCoderCarl&layout=compact&count_private=true&theme=gruvbox)

<!--
**CoolCoderCarl/CoolCoderCarl** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

<!--
https://github.com/progfay/shields-with-icon
-->
